# Rúben André Barreiro | GitHub's portfolio/personal blog

A **_web-based portfolio_** designed and developed to illustrate some of **_hard skills_**, **_soft skills_**, **_competences_**, **_hobbies_**, **_important documents_**, among many others.

Here will be shown some of **_my academic, professional and personal works_**, as some of **_my photography and modeling photoshoot's works_**, among many others.

Brief description about important information which will be addressed in this portfolio:

- Academic projects developed during my **_Integrated Master (BSc./MSc.) degree at [FCT NOVA](https://www.fct.unl.pt/)_**;
- Scientific research projects developed during my **_Integrated Master (BSc./MSc.) degree at [FCT NOVA](https://www.fct.unl.pt/)_**;
- Academic projects developed at **_[FEUP](https://www.fe.up.pt/)_**;
- Personal projects developed, **_in my free times_**, as **_hobbies_**;
- **_Hard skills_** and **_soft skills_** learned, as also, all the **_full competences_**, acquired during my academic process;
- Personal **_photography's works_**;
- Personal **_modeling photoshoot's works_**;

Finally, will be described some of my personal story's background and some curiosities about it.
I hope you enjoy it!

You can view my **_web-based portfolio_** _online_, [clicking here](https://rubenandrebarreiro.github.io/)!

_Thank you very much,_

_Kind regards!_

**_Rúben André Letra Barreiro_**

## Screenshots

![https://raw.githubusercontent.com/rubenandrebarreiro/rubenandrebarreiro.github.io/master/assets/images/screenshots/screenshot-1.jpg](https://raw.githubusercontent.com/rubenandrebarreiro/rubenandrebarreiro.github.io/master/assets/images/screenshots/screenshot-1.jpg)
######  Rúben André Barreiro | GitHub's portfolio/personal blog - Screenshot #1

***

![https://raw.githubusercontent.com/rubenandrebarreiro/rubenandrebarreiro.github.io/master/assets/images/screenshots/screenshot-2.jpg](https://raw.githubusercontent.com/rubenandrebarreiro/rubenandrebarreiro.github.io/master/assets/images/screenshots/screenshot-2.jpg)
######  Rúben André Barreiro | GitHub's portfolio/personal blog - Screenshot #2

***

![https://raw.githubusercontent.com/rubenandrebarreiro/rubenandrebarreiro.github.io/master/assets/images/screenshots/screenshot-3.jpg](https://raw.githubusercontent.com/rubenandrebarreiro/rubenandrebarreiro.github.io/master/assets/images/screenshots/screenshot-3.jpg)
######  Rúben André Barreiro | GitHub's portfolio/personal blog - Screenshot #3

***

![https://raw.githubusercontent.com/rubenandrebarreiro/rubenandrebarreiro.github.io/master/assets/images/screenshots/screenshot-4.jpg](https://raw.githubusercontent.com/rubenandrebarreiro/rubenandrebarreiro.github.io/master/assets/images/screenshots/screenshot-4.jpg)
######  Rúben André Barreiro | GitHub's portfolio/personal blog - Screenshot #4

***

![https://raw.githubusercontent.com/rubenandrebarreiro/rubenandrebarreiro.github.io/master/assets/images/screenshots/screenshot-5.jpg](https://raw.githubusercontent.com/rubenandrebarreiro/rubenandrebarreiro.github.io/master/assets/images/screenshots/screenshot-5.jpg)
######  Rúben André Barreiro | GitHub's portfolio/personal blog - Screenshot #5

***

![https://raw.githubusercontent.com/rubenandrebarreiro/rubenandrebarreiro.github.io/master/assets/images/screenshots/screenshot-6.jpg](https://raw.githubusercontent.com/rubenandrebarreiro/rubenandrebarreiro.github.io/master/assets/images/screenshots/screenshot-6.jpg)
######  Rúben André Barreiro | GitHub's portfolio/personal blog - Screenshot #6

***

## Contacts

* **Personal e-mails:** [ruben.barreiro.92@outlook.com](mailto:ruben.barreiro.92@outlook.com) | [ruben.barreiro.92@gmail.com](mailto:ruben.barreiro.92@gmail.com) | [rubenbarreiro92@yahoo.com](mailto:rubenbarreiro92@yahoo.com)
* **Academic e-mails @ FCT NOVA:** [r.barreiro@campus.fct.unl.pt](mailto:r.barreiro@campus.fct.unl.pt)
* **Academic e-mails @ FEUP:** [up201808917@fe.up.pt](mailto:up201808917@fe.up.pt) | [up201808917@g.uporto.pt](mailto:up201808917@g.uporto.pt)
* **Mobile phone:** +351 911 097 424

#### Notes

- Inspired in a Jekyll theme. Demo: <http://redvi.github.io/voyager>
